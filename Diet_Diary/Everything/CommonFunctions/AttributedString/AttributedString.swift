//
//  AttributedString.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class AttributedString {
    
    static let shared = AttributedString()
    
    func attributeString(value: String, font: String, size: Int) -> NSMutableAttributedString {
        let string = NSMutableAttributedString(string: value, attributes: [NSFontAttributeName: UIFont(name: font,size: CGFloat(size)) as Any])
        return string
    }
    
}
