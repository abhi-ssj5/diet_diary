//
//  CollectionViewCell.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblTimeSlot: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    var cellData:Category? {
        didSet {
            updateCell()
        }
    }
    
    func updateCell() {
        colorView.backgroundColor = cellData?.color
        lblCategoryName.text = cellData?.name
        if (cellData?.start_time?.isEmpty)! {
            lblTimeSlot.text = ""
        } else {
            lblTimeSlot.text = (cellData?.start_time)! + " to " + (cellData?.end_time)!
        }
        
        let combination = NSMutableAttributedString()
        combination.append(AttributedString.shared.attributeString(value: (cellData?.value ?? "0"), font: "Lato-Light", size: 36))
        combination.append(AttributedString.shared.attributeString(value: " Kcal", font: "Lato-Regular", size: 14))
 
        lblValue.attributedText = combination
    }
    
    override func awakeFromNib() {
        self.colorView.layer.cornerRadius = self.colorView.frame.height / 2
    }
    
}
