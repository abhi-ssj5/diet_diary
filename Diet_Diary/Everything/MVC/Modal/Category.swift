//
//  Category.swift
//  Diet_Diary
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class Category {
    
    var id:String?
    var name:String?
    var start_time:String?
    var end_time:String?
    var value:String?
    var color:UIColor?
    
    init(id: String, name: String, start_time: String, end_time: String, value: String = "0", color: UIColor = UIColor.white.withAlphaComponent(1)) {
        self.id = id
        self.name = name
        self.start_time = start_time
        self.end_time = end_time 
        self.value = value
        self.color = color
    }
    
}
